# My presonal code review guidelines #

* [Tips and tricks for common tasks](codereview/wiki/Tips)

* [What I check in code review](codereview/wiki/Review)

* [POWA coding standards](https://docs.powa.com/confluence/display/DEVELOPMENT/CodingStandards)

* [JSON and curl](/JPavlicek/codereview/wiki/json)

* [PowaTag guideline](https://docs.powa.com/confluence/display/FED/Powatag+JavaScript+Implementation#PowatagJavaScriptImplementation-Choosingtheoptions)

* [API 1.2 doc](https://mpayme.atlassian.net/wiki/display/PPD/API+1.2)

* [Setup new PT2](https://docs.powa.com/confluence/display/PT2/Implementation)

* [QR code sandbox](http://test.powaweb.io/powatag/2.3.0/)

